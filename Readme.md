# Main Framework

Steps:
1. Git clone the repository
2. Delete .git folder (linux: `sudo rm -rf .git`)
3. Make all the changes needed
4. Create new repository in gitlab without Readme file and follow the steps for "Uploading existing project"

## Run in local
- Make bash script executable: `chmod u+x ./build.sh`. *OBS:* This is only for linux & has to be in the same directory as the script.

Or:
- Create virtual env
'''
python3 -m venv venv
source venv/bin/activate
'''
- Install main dependencies
'''
pip install -r requirements.txt
'''

#!/bin/bash

sudo git pull
sudo rm -rf .git
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt


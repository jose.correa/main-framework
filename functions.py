# Libraries                
import google.auth
from google.cloud import bigquery
from google.cloud import bigquery_storage
# Properties                  
import properties as p
import requests
import json
import pandas as pd
import time
from rich import print
from rich.console import Console
console = Console()


credentials, your_project_id = google.auth.default(
    scopes=['https://www.googleapis.com/auth/bigquery', 
        'https://www.googleapis.com/auth/cloud-platform', 
        'https://www.googleapis.com/auth/drive']
)

bqclient = bigquery.Client(credentials=credentials, project=p.main_project_id,)


# Functions   

## Bigquery to pandas DF               
def bq_query(query) -> pd.DataFrame:
   """ Run query in BigQuery"""
   console.log('Executing query.')
   results = bqclient.query(query).to_dataframe()
   return results

def df_to_bq_table(dataframe, write_disposition, project, dataset, table):
    job_config = bigquery.LoadJobConfig(
        source_format=bigquery.SourceFormat.CSV,
        schema=[]
    )
    job_config.create_disposition = bigquery.CreateDisposition.CREATE_IF_NEEDED
    job_config.write_disposition = write_disposition
    job = bqclient.load_table_from_dataframe(dataframe,
                                           '{}.{}'.format(dataset, table),
                                           job_config=job_config)
    job.result()
    console.log("Loaded {} rows into {}.{}.{}.".format(job.output_rows,
                                                      project,
                                                      dataset,
                                                      table))


class Hangouts:
    def __init__(self, 
                project_name=p.hangouts_project_name, 
                print_color='#E88694', 
                url=p.hangouts_url, 
                hangouts_status=p.hangouts_status,
                print_datetime=True):
        self.url = url
        self.thread = ''
        self.hangouts_status = hangouts_status
        self.project_name = project_name
        self.project_name_print_color = print_color
        self.print_datetime = print_datetime
        self.__prefix = self.set_prefix()

    def set_prefix(self) -> str:
      """ Set prefix for hangouts messages """
      if self.print_datetime:
         return f'*{time.strftime("%Y-%m-%d %H:%M:%S")}: [{self.project_name}]* - '
      else:
         return f'*[{self.project_name}] - *'

    def send_message(self, message):
      """ Send normal message to Hangouts. 
         If prefix enable, its going to add prefix @ start
      """
      console.log("Sending message to hangouts:", message)
      if self.hangouts_status:
         bot_message = {
                     'text' : self.__prefix + message
                  }
         if self.thread != '':
               bot_message['thread'] = self.thread
         response = requests.post(
               self.url,
               json=bot_message,
         )
         if response.ok:
               self.thread = json.loads(response.content.decode("utf-8"))['thread']
         else:
               console.log(f"Error ({response.status_code}) sending the message: {response.content}", style="red")
      else:
         console.log("Hangouts disabled. Message not sent.")

    def send_text_card(self, title: str, paragraph: str):
      """ Send a text card with a title and a paragraph 
         to hangouts. 
         Paragraphs can have styled messages in them 
         (font color, links, underline, btw other)
         https://developers.google.com/chat/api/guides/message-formats/cards#card_text_formatting
      """
      console.log(f"Sending new card message to hangouts: Title '{title}'")
      if self.hangouts_status:
         if title is not None:
               header = {
                  'title': title,
               }
         else:
               header = {}
         widget = {'textParagraph': {'text': paragraph}}
         response = requests.post(
                              self.url,
                              json={
                                 'cards': [
                                       {
                                          'header': header,
                                          'sections': [{'widgets': [widget]}],
                                       }
                                 ]
                              },
                              )
         if response.ok:
               try:
                  self.thread = json.loads(response.content.decode("utf-8"))['thread']
               except Exception:
                  console.log(f"Error finding sended thread")
         else:
               console.log(f"Error ({response.status_code}) sending the message: {response.content}", style="red")
      else:
         console.log("Hangouts disabled. Message not sent.")

    def send_link_card(self, title: list, url: list, icon: str):
       """ Send a link card style to hangouts
         For more info: https://developers.google.com/chat/api/guides/message-formats/cards#card_text_formatting
      """
      console.log(f"Sending new card message to hangouts: Title '{title}'")
      if self.hangouts_status:
         response = requests.post(
                              self.url,
                              json={
                                 "cards": [
                                 {
                                          "sections": [
                                             {
                                             "widgets": [
                                                      {
                                                         "keyValue": {
                                                         "topLabel": title[0],
                                                         "content": title[1],
                                                         "contentMultiline": "false",
                                                         "bottomLabel": title[2],
                                                         "onClick": {
                                                                  "openLink": {
                                                                     "url": url[1]
                                                                  }
                                                               },
                                                         "icon": icon,
                                                         "button": {
                                                                  "textButton": {
                                                                     "text": url[0],
                                                                     "onClick": {
                                                                     "openLink": {
                                                                              "url": url[1]
                                                                     }
                                                                     }
                                                                  }
                                                               }
                                                         }
                                                      }
                                             ]
                                             }
                                          ]
                                 }
                                 ]
                              }
                              )
         if response.ok:
               try:
                  self.thread = json.loads(response.content.decode("utf-8"))['thread']
               except Exception:
                  console.log(f"Error finding sended thread")
         else:
               console.log(f"Error ({response.status_code}) sending the message: {response.content}", style="red")
      else:
         console.log("Hangouts disabled. Message not sent.")

